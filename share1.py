import selenium
import re
import os
import csv
import multiprocessing
import requests

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

home = os.environ['HOME']


# inizializzo file di storico
def createhistory():
    if not os.path.isfile(home + r'/Downloads/share/history.csv'):
        with open(home + r'/Downloads/share/history.csv', 'w', newline='') as csv_create:
            mem = csv.writer(csv_create)
            mem.writerow(['app', 'downloaded', 'downloads'])
            csv_create.close()


# calcolo pagina max
def lastpage():
    driver = selenium.webdriver.Firefox(executable_path='/home/marco/Downloads/geckodriver/geckodriver')
    driver.get(str("https://shareappscrack.com/"))
    maxpage = 0
    max = driver.find_elements_by_tag_name('a')
    for var in max:
        var.get_attribute('href')
        pattern = re.compile(str('https://shareappscrack.com/page/').lower())
        if pattern.search(str(var.get_attribute('href')).lower()):
            temp1 = str(var.get_attribute('href')).replace('/', '')
            temp2 = str(temp1).replace('https:shareappscrack.compage', '')
            if int(maxpage) < int(temp2):
                maxpage = int(temp2)
    driver.close()
    return maxpage


# apro file e restituisco array
def memarray():
    result = {}
    with open(home + r'/Downloads/share/history.csv', 'r') as csv_memory:
        mem = csv.DictReader(csv_memory)
        result['app'] = [row['app'] for row in mem]
        csv_memory.seek(0)
        next(csv_memory)
        result['downloaded'] = [row['downloaded'] for row in mem]
        csv_memory.seek(0)
        next(csv_memory)
        result['downloads'] = [row['downloads'] for row in mem]
        csv_memory.close()
    return result


# scrivo nell'array
def popolaarray(mem, link, downLinks):
    addelement = True
    for element in mem['app']:
        pattern = re.compile(str(link).lower())
        if pattern.search(str(element.lower())):
            addelement = False
    if addelement:
        mem['app'].append(str(link))
        mem['downloaded'].append(0)
        mem['downloads'].append('')
    if downLinks:
        i = 1
        while i <= len(mem):
            pattern = re.compile(str(link).lower())
            if pattern.search(str(mem['app'].lower())):
            #if mem['app'] == link:
                mem['downloads'] = downLinks
                i += 1

# rileva i link e li scrive nell'array
def linksearcher(maxpag):
    history = memarray()
    driver = selenium.webdriver.Firefox(executable_path='/home/marco/Downloads/geckodriver/geckodriver')
    i = 264
    while i <= maxpag:  # per ogni pagina
        driver.get('https://shareappscrack.com/page/' + str(i))
        waiter = WebDriverWait(driver, 1).until(
            expected_conditions.presence_of_all_elements_located((By.TAG_NAME, 'a')))
        list = driver.find_elements_by_tag_name('a')
        ele = [listele.get_attribute('href') for listele in list]
        if i == maxpag:
            links = ele[61:int((len(ele) - 15))]
            for link in links:
                #print(str(link))
                popolaarray(history, link, '')
        else:
            links = ele[61:71]
            for link in links:
                #print(str(link))
                popolaarray(history, link, '')
        i += 1
    syncCsv(history)
    driver.close()


# fa la sync del csv, aggiungendo ciò che manca rispetto alla scan
def syncCsv(array):
    with open(home + r'/Downloads/share/history.csv', 'w') as csv_writer:
        mem = csv.writer(csv_writer)
        mem.writerow(['app','downloaded','downloads'])
        iteratorApp=iter(array['app'])
        iteratorDownloaded=iter(array['downloaded'])
        iteratorDownloads=iter(array['downloads'])
        i = 1
        while i <= len(array['app']):
            mem.writerow([next(iteratorApp),next(iteratorDownloaded),next(iteratorDownloads)])
            i += 1
    csv_writer.close()
    #csv_memory.close()




# recupera i download dalla pagina
def getDownloadLinks(page):
    response = requests.get(page + '?download=show')
    body = response.text
    print(str(page + '?download=show'))
    body = body[body.find('<div class="show_download_links"'):]
    # body = body[:body.find("</tbody>")]
    body = body[:body.find("</div>")]
    # print(str(body))
    elementLinks = re.findall('<a href="', body)
    arr = []
    for ele in elementLinks:
        # print(body)
        arr.append(body[body.find('<a href="'):body.find('" target="_blank"')].replace('<a href="', ''))
        body = body[body.find('" target="_blank"') + 17:]
    return arr


# chiama la funzione recupera download dalla pagina
def linkCaller(array):
    for element in array['app']:
        popolaarray(array, element, getDownloadLinks(element))


createhistory()
#linksearcher(lastpage())
linksearcher(268)

